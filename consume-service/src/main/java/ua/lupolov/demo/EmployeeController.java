package ua.lupolov.demo;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@Slf4j
@RestController
@RequiredArgsConstructor
public class EmployeeController {

    private final EmployeeRepository repository;

    @PostMapping(value = "/employees")
    public ResponseEntity<?> saveEmployees(@RequestBody List<EmployeeCreateDto> employees) {

        List<Employee> savedEmployees = repository.saveAll(toEmployeeList(employees));

        log.info("Saved new employee: {}", savedEmployees);

        return ResponseEntity.ok().build();
    }

    private List<Employee> toEmployeeList(List<EmployeeCreateDto> employeeCreateDtos) {

        List<Employee> employees = new ArrayList<>();

        for (EmployeeCreateDto employeeCreateDto : employeeCreateDtos) {
            Employee employee = toEmployee(employeeCreateDto);
            employees.add(employee);
        }

        return employees;
    }

    private Employee toEmployee(EmployeeCreateDto createDto) {

        if (createDto == null) {
            return null;
        }

        var employee = new Employee();
        employee.setFullName(createDto.getFullName());
        employee.setAge(createDto.getAge());
        employee.setBirthday(createDto.getBirthday());

        return employee;
    }

}
