package ua.lupolov.demo.batch.job.step1;

import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import ua.lupolov.demo.EmployeeCreateDto;
import ua.lupolov.demo.service.RestService;

import java.util.List;

@Slf4j
@Component
public class EmployeeListWriter implements ItemWriter<EmployeeCreateDto> {

    @Value("http://localhost:8081/employees")
    private String consumeServiceUrl;

    private final RestService restService;

    public EmployeeListWriter(RestService restService) {
        this.restService = restService;
    }

    @Override
    public void write(List<? extends EmployeeCreateDto> items) throws Exception {
        ResponseEntity<?> responseEntity
                = restService.sendPostForList(consumeServiceUrl, new LinkedMultiValueMap<>(), items);

        if (responseEntity.getStatusCode().is2xxSuccessful()) {
            log.info("Employees wrote successfully. Size: {}", items.size());
        }
    }
}
