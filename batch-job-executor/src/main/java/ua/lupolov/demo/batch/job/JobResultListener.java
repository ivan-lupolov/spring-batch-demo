package ua.lupolov.demo.batch.job;

import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobExecutionListener;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class JobResultListener implements JobExecutionListener {
    @Override
    public void beforeJob(JobExecution jobExecution) {
        log.info("Called beforeJob() jobExecution:{}", jobExecution.toString());
    }

    @Override
    public void afterJob(JobExecution jobExecution) {
        log.info("Called afterJob() jobExecution:{}", jobExecution.toString());
    }
}
