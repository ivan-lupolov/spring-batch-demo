package ua.lupolov.demo.batch.job.step1;

import lombok.RequiredArgsConstructor;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.stereotype.Component;
import ua.lupolov.demo.EmployeeCreateDto;
import ua.lupolov.demo.Person;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;

@Component
@RequiredArgsConstructor
public class PersonToEmployeeProcessor implements ItemProcessor<Person, EmployeeCreateDto> {

    @Override
    public EmployeeCreateDto process(Person item) throws Exception {
        return toEmployeeCreateDto(item);
    }

    private EmployeeCreateDto toEmployeeCreateDto(Person person) {

        if (person == null) {
            return null;
        }

        var dto = new EmployeeCreateDto();

        String fullName = person.getFirstName() + " " + person.getLastName();
        dto.setFullName(fullName);

        LocalDate birthday = LocalDate.of(person.getYearOfBirth(), person.getMonthOfBirth(), person.getDayOfBirth());
        dto.setBirthday(birthday);

        LocalDate now = LocalDate.now();
        long age = ChronoUnit.YEARS.between(birthday, now);
        dto.setAge((int) age);

        return dto;
    }
}
