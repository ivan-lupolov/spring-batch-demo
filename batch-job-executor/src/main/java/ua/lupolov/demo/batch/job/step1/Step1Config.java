package ua.lupolov.demo.batch.job.step1;

import lombok.RequiredArgsConstructor;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import ua.lupolov.demo.EmployeeCreateDto;
import ua.lupolov.demo.Person;

@Configuration
@RequiredArgsConstructor
public class Step1Config {

    public static final int CHUNK_SIZE = 2;

    private final PersonListReader reader;
    private final PersonToEmployeeProcessor processor;
    private final EmployeeListWriter writer;
    private final StepBuilderFactory stepBuilderFactory;

    @Bean
    public Step step1() {
        return stepBuilderFactory.get("step1")
                .<Person, EmployeeCreateDto>chunk(CHUNK_SIZE)
                .reader(reader)
                .processor(processor)
                .writer(writer)
                .startLimit(Integer.MAX_VALUE)
                .allowStartIfComplete(true)
                .build();
    }
}
