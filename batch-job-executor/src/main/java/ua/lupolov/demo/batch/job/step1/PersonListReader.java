package ua.lupolov.demo.batch.job.step1;

import org.springframework.batch.item.ItemReader;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import ua.lupolov.demo.Person;
import ua.lupolov.demo.service.RestService;

import java.util.Collections;
import java.util.List;

@Component
public class PersonListReader implements ItemReader<Person> {

    @Value("http://localhost:8080/persons")
    private String produceServiceUrl;

    private List<Person> persons;

    private final RestService restService;

    public PersonListReader(RestService restService) {
        this.restService = restService;
    }

    @Override
    public Person read() {

        if (persons == null) {
            ResponseEntity<List<Person>> personResponseEntity
                    = restService.sendGetForList(produceServiceUrl, new LinkedMultiValueMap<>(), Person.class);

            if (personResponseEntity.getStatusCode().is2xxSuccessful()) {
                persons = personResponseEntity.getBody();
            } else {
                persons = Collections.emptyList();
            }
        }

        if (!persons.isEmpty()) {
            return persons.remove(0);
        }

        return null;
    }
}
