package ua.lupolov.demo.service;

import org.springframework.http.ResponseEntity;
import org.springframework.util.MultiValueMap;

import java.util.List;

public interface RestService {

    <T> ResponseEntity<List<T>> sendGetForList(String exchangeUrl, MultiValueMap<String, String> headers, Class<T> targetType);

    <T> ResponseEntity<?> sendPostForList(String exchangeUrl, MultiValueMap<String, String> headers, List<T> body);
}
