package ua.lupolov.demo.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.stereotype.Component;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@Slf4j
@Component
@RequiredArgsConstructor
public class RestServiceImpl implements RestService {

    private final RestTemplate restTemplate;

    @Override
    public <T> ResponseEntity<List<T>> sendGetForList(String exchangeUrl, MultiValueMap<String, String> headers, Class<T> targetType) {
        var requestHeaders = new HttpHeaders(headers);
        requestHeaders.setContentType(MediaType.APPLICATION_JSON);

        var httpEntity = new HttpEntity<>(requestHeaders);
        try {
            return restTemplate.exchange(exchangeUrl, HttpMethod.GET, httpEntity, new ParameterizedTypeReference<List<T>>() {});
        }
        catch (Exception e) {
            log.error("Error during reading persons", e);
            return null;
        }
    }

    @Override
    public <T> ResponseEntity<?> sendPostForList(String exchangeUrl, MultiValueMap<String, String> headers, List<T> body) {
        var requestHeaders = new HttpHeaders(headers);
        requestHeaders.setContentType(MediaType.APPLICATION_JSON);

        var httpEntity = new HttpEntity<>(body, requestHeaders);

        try {
            return restTemplate.exchange(exchangeUrl, HttpMethod.POST, httpEntity, new ParameterizedTypeReference<>() {});
        }
        catch (Exception e) {
            log.error("Error during writing employees", e);
            return ResponseEntity.internalServerError().build();
        }
    }
}
