package ua.lupolov.demo;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Slf4j
@RestController
public class ProduceController {

    private static final List<Person> PEOPLE = List.of(

            Person.builder()
                    .firstName("Lupolov")
                    .lastName("Ivan")
                    .dayOfBirth(25)
                    .monthOfBirth(7)
                    .yearOfBirth(1993)
                    .build(),

            Person.builder()
                    .firstName("Seleznev")
                    .lastName("Alexandr")
                    .dayOfBirth(12)
                    .monthOfBirth(5)
                    .yearOfBirth(1993)
                    .build(),

            Person.builder()
                    .firstName("Yemelyanov")
                    .lastName("Denis")
                    .dayOfBirth(31)
                    .monthOfBirth(8)
                    .yearOfBirth(1989)
                    .build(),

            Person.builder()
                    .firstName("Lapin")
                    .lastName("Aleksey")
                    .dayOfBirth(2)
                    .monthOfBirth(12)
                    .yearOfBirth(1980)
                    .build(),

            Person.builder()
                    .firstName("Sharygin")
                    .lastName("Victor")
                    .dayOfBirth(16)
                    .monthOfBirth(3)
                    .yearOfBirth(1965)
                    .build()
    );

    @GetMapping("/persons")
    public ResponseEntity<List<Person>> getAllPersons() {
        log.info("Get persons chunk. Size: {}", PEOPLE.size());
        return ResponseEntity.ok(PEOPLE);
    }

}
